#include "IKitten.h"

meow::IKitten::IKitten()
{
}

size_t meow::IKitten::getSpeed()
{
	return this->speed;
}

void meow::IKitten::setSpeed(size_t speed)
{
	this->speed = speed;
}

size_t meow::IKitten::getEatSpeed()
{
	return this->eatSpeed;
}

void meow::IKitten::setEatSpeed(size_t eatSpeed)
{
	this->eatSpeed = eatSpeed;
}

size_t meow::IKitten::getStomachVolume()
{
	return this->stomachVolume;
}

void meow::IKitten::setStomachVolume(size_t stomachVolume)
{
	this->stomachVolume = stomachVolume;
}

meow::KittenAction& meow::IKitten::getAction()
{
	return this->action;
}

void meow::IKitten::setAction(KittenAction & action)
{
	this->action = action;
}

meow::IKitten::~IKitten()
{
}
