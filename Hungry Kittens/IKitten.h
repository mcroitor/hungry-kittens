#ifndef KITTEN_H_
#define KITTEN_H_

#include <iostream>

namespace meow {

	enum KittenAction {
		MOVE_ACTION,
		ROTATE_ACTION,
		EAT_ACTION,
		SLEEP_ACTION
		// TODO: COMPOSED_ACTION
	};

	class IKitten {
		size_t speed;
		size_t eatSpeed;
		size_t stomachVolume;
		float sleepTime;
		KittenAction action;
	public:
		IKitten();
		size_t getSpeed();
		void setSpeed(size_t);
		size_t getEatSpeed();
		void setEatSpeed(size_t);
		size_t getStomachVolume();
		void setStomachVolume(size_t);
		KittenAction & getAction();
		void setAction(KittenAction &);
		virtual void hunt() = 0;
		virtual ~IKitten();
	};

}

#endif