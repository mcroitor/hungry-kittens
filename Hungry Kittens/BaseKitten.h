#ifndef BASE_KITTEN_H_
#define BASE_KITTEN_H_

#include "IKitten.h"

#ifndef KITTEN_EXPORT
#define KITTEN_API __declspec(dllexport)
#else
#define KITTEN_API __declspec(dllimport)
#endif

namespace meow {
	class BaseKitten : public IKitten {
	public:
		BaseKitten();
		virtual void hunt();
		~BaseKitten();
	};

	KITTEN_API IKitten * getKitten();
}

#endif